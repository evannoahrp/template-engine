const express = require("express");
const router = express.Router();
const pagesRouter = require("./pagesRouter");
const usersRouter = require("./usersRouter");

router.use("/", pagesRouter);
router.use("/users", usersRouter);

module.exports = router;
