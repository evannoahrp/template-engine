const router = require("express").Router();
const pagesController = require("../controllers/pagesController");

router.get("/", pagesController.home);
router.get("/about", pagesController.about);
router.get("/articles", pagesController.articles);

module.exports = router;
