const { Article } = require("../models");

module.exports = {
  home: (req, res) => {
    const title = "Hello World",
      subTitle = "Welcome to the world!";

    res.render("index", { title, subTitle });
  },

  about: (req, res) => {
    const title = "About";

    res.render("about", { title });
  },

  articles: (req, res) => {
    const title = "Articles";

    Article.findAll({}).then((articles) => {
      res.render("articles", { title, articles });
    });
  },
};
